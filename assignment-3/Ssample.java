/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssample;


public class Ssample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       CannotFly cannotFly = new CannotFly();
       CannotSwim cannotSwim = new CannotSwim();
       CanFly canFly = new CanFly();
       CanSwim canSwim = new CanSwim();
       CanHover canHover = new CanHover();
       CanDive canDive = new CanDive();
             
       Duck duck = new Duck(cannotFly, canSwim);
       duck.fly();
       duck.swim();
       
       Penguin penguin = new Penguin(cannotFly, canSwim);
       penguin.fly();
       penguin.swim();
       
       Chicken chicken = new Chicken(cannotFly, cannotSwim);
       chicken.fly();
       chicken.swim();
       
       HummingBird hummingbird = new HummingBird(canHover, cannotSwim);
       hummingbird.fly();
       hummingbird.swim();
       
       Seagull seagull = new Seagull(canFly, canDive);
       seagull.fly();
       seagull.swim();
    }
    
}
